const express = require('express')
const app = express()
var port = process.env.PORT || 8080;
app.use(express.json({extended: false}))
const cors = require('cors');
app.use(cors());
app.listen(port)
console.log("User account Microservice is running on port " + port);
const MongoClient = require('mongodb').MongoClient;
const mongourl = "mongodb+srv://cca-kumarp14-gembalis1:sprint2@cca-kumarp14-gembalis1.t05ka.mongodb.net/sprint2-teamproject?retryWrites=true&w=majority"; //the full URL with username/password
const dbClient = new MongoClient (mongourl, {useNewUrlParser: true, useUnifiedTopology: true});

dbClient.connect (err => {
    if (err) throw err;
    console.log ("Connected to the MongoDB cluster");
});
app.get("/", (req,res)=>{
    res.sendFile(__dirname+"/signuptest.html");
})


app.get('/login/:username/:password', (req,res) => {
    console.log('Debug: /login route is handling the request');
    const db = dbClient.db();
    const username = req.params.username;
    const password = req.params.password;
    if(username.length < 5 || !username)
    {
        return res.send({status: "Invalid", message: "You have entered Invalid credentials..."});
    }
    if(password.length < 5 || !password)
    {
        return res.send({status: "Invalid", message: "You have entered Invalid credentials..."});
    }
    db.collection("users").
    findOne({username:username, password:password}, (err, user)=>{
        if(err || !user){
            console.log(`${username}/${password} not found...!`);
            return res.send({status: "notfound", message: "Invalid login credentials...!"});
        }
        console.log(`user.username: ${user.username}; user.password:${password}`);
        if(user && user.username == username)
        {
            let account = {username: user.username,
            fullname:user.fullname,
            email: user.email};
            console.log(`${username}/${password}, found! account: ` + JSON.stringify(account));
            return res.status(200).json({status:"authenticated", profile: {username:user.username,fullname:user.fullname}});
        }
    });
});  

app.post('/signup', (req,res)=> {
    console.log('Debug: /signup route is handling the request.req.body;' +JSON.stringify(req.body));
    const db = dbClient.db();
    const {username, password, fullname, email} = req.body;
    console.log('/signup -> req.body.username: '+username);
    console.log('/signup -> req.body.password: '+password);
    if(!username || username.length < 5) {
        return res.send({status: "Invalid", message: "Invalid Credentials..!"});
    }
    if(!password || password.length < 5){
        return res.send({status: "Invalid", message: "Invalid Credentials...!"});
    }
    db.collection("users").findOne({username:username}, (err,user)=>{
        console.log('/signup->req.body.username: '+JSON.stringify(username));
        if(user){
            return res.send({status: "Invalid", message: "you are already registered with us, you can login anytime!"});
        }else{
            let newUser = {username, password, fullname, email};
            db.collection("users").insertOne(newUser,(err,result)=>{
            if(err){
               return res.send({status: "Invalid", message: err.message});
            }
                return  res.send({status: "Registered", message: "User successfully registered with us, you can login now!"});
            });
            
        }
    })
    
}) 